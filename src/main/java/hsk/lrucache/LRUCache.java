package hsk.lrucache;

import java.util.LinkedHashMap;
import java.lang.instrument.Instrumentation;

public class LRUCache<K, V> {

    private LinkedHashMap<K, V> objLinkedHashMap;
    private static Instrumentation instrumentation;
    // total cache size is the total size of "values" added in the map in bytes
    private long cacheSize;
    private long maxCacheSize;

    public LRUCache(long maxCacheSize)
    {
        objLinkedHashMap = new LinkedHashMap();
        this.maxCacheSize = maxCacheSize;
    }

    public void Add(K key, V value)
    {
        objLinkedHashMap.put(key, value);
        // increase the cache size
        cacheSize += instrumentation.getObjectSize(value);
    }

    public V get(K key)
    {
        return objLinkedHashMap.get(key);
    }

    public void remove(K key)
    {
        // get the value and its size
        V value = objLinkedHashMap.get(key);
        cacheSize -= instrumentation.getObjectSize(value);
        objLinkedHashMap.remove(key);
    }

    public long getCacheSize()
    {
        return cacheSize;
    }
}